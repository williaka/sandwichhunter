package com.example.sandwichhunter;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class Tools {
    // To draw the text in the center of the screen
    public static Rect r = new Rect();

    // get the screen dimensions
    public static int mScreenX, mScreenY;

    public Tools(int mScreenX, int mScreenY) {
        this.mScreenX = mScreenX;
        this.mScreenY = mScreenY;
    }

    // draw Center Text (on the internet)
    // see drawCenteredText
    public static void drawCenterText(Canvas canvas, Paint paint, String text) {
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y = cHeight / 2f + r.height() / 2f - r.bottom;
        canvas.drawText(text, x, y, paint);
    }

    /**
     * on the internet
     * draw a text centered around a position (coordX, coordY)
     * @param canvas : the application's canvas
     * @param paint : the application's paint
     * @param text  : the text to draw
     * @param coordX : the X-position of the center of the text
     * @param coordY : the Y-position of the center of the text
     */
    public static void drawCenteredText(Canvas canvas, Paint paint, String text, float coordX, float coordY) {
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        float x = coordX - r.width() / 2f - r.left;
        float y = coordY + r.height() / 2f - r.bottom;
        canvas.drawText(text, x, y, paint);
    }

    /*
    just like the drawCenteredText
    the only difference is that this method draws the text vertically
     */
    public static void drawCenteredTextVertically(Canvas canvas, Paint paint, String text, float x, float y){
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        if(x < 0)
            x = cWidth / 2f - r.width() / 2f - r.left;
        if (y < 0)
            y = cHeight / 2f + r.height() / 2f - r.bottom;
        canvas.rotate(-90,x,y);
        canvas.drawText(text, x, y, paint);
        canvas.rotate(90,x,y);
    }

    /**
     *
     * @param mCanvas : the application's canvas
     * @param mPaint : the application's paint
     * @param x : the X-position of the left point of the base of the triangle
     * @param y : the Y-position of the left point of the base of the triangle
     * @param width : the width of the base of the triangle
     * @param height : the height of the triangle
     * @param inverted : whether the triangle should be drawn inverted or not
     *                 if true, the base of the triangle will be at the top
     */
    public static void drawTriangle(Canvas mCanvas, Paint mPaint, int x, int y, int width, int height, boolean inverted) {
        Point p1 = new Point(x,y);
        int pointX = x + width/2;
        int pointY = inverted?  y + height : y - height;

        Point p2 = new Point(pointX,pointY);
        Point p3 = new Point(x+width,y);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(p1.x,p1.y);
        path.lineTo(p2.x,p2.y);
        path.lineTo(p3.x,p3.y);
        path.close();

        mCanvas.drawPath(path, mPaint);

    }

    /**
     * this method draws the view field where the player can shoot
     * @param mCanvas
     * @param mPaint
     * @param x : the X-position of the top of the player
     * @param y : the Y-position of the top of the player
     * @param width : the width of field (used in drawTiangle)
     * @param height : the height of field (used in drawTiangle)
     * @param angle : the angle the should be exluded from the view field
     *              in our case it represents the blindArea
     * @param inverted : whether the field is inverted or not
     */
    public static void drawViewField(Canvas mCanvas, Paint mPaint, int x, int y, int width, int height, float angle, boolean inverted) {
        if (height == -1)
            height = (int) (width/2 * Math.sin(angle));

        // Change the drawing color to white
        mPaint.setColor(Color.argb(150, 255, 255, 255));
        mPaint.setShader(new LinearGradient(0, 0, 0, height, Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));
        drawTriangle(mCanvas, mPaint, x - width/2, y - height, width, height, !inverted);

        //drawHalfCircle(mCanvas, mPaint, x, y - height, inverted);
        mCanvas.drawArc(x - width/2, y - 2 * height, x + width/2, y, 180f, 180f, true, mPaint);

        mPaint.setColor(Color.argb(255, 255, 0, 0));
        mPaint.reset();
        //mCanvas.drawLine(0, y - height, mScreenX, y - height, mPaint);
    }

    /**
     * draw a half circle (on the internet)
     * not used in the program
     * @param mCanvas
     * @param mPaint
     * @param mX
     * @param mY
     * @param inverted
     */
    public static void drawHalfCircle(Canvas mCanvas, Paint mPaint, int mX, int mY, boolean inverted) {
        /*
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(50);
        // Setting the color of the circle
        mPaint.setColor(Color.BLUE);

        float mX = mScreenX/2f;
        float mY = mScreenY/2f;

        // Draw the circle at (x,y) with radius 250
        int radius = 250;
        mCanvas.drawCircle(mX, mY, radius, mPaint);

        mPaint.setColor(Color.YELLOW);
        mPaint.setDither(true);                    // set the dither to true
        mPaint.setStyle(Paint.Style.STROKE);       // set to STOKE
        mPaint.setStrokeJoin(Paint.Join.ROUND);    // set the join to round you want
        mPaint.setStrokeCap(Paint.Cap.ROUND);      // set the paint cap to round too
        mPaint.setPathEffect(new CornerPathEffect(50) );   // set the path effect when they join.
        mPaint.setAntiAlias(true);

        RectF oval = new RectF(mX - radius, mY - radius, mX + radius, mY + radius);
        mCanvas.drawArc(oval, -90, 90, false, mPaint);
        mPaint.setColor(Color.RED);
        mCanvas.drawArc(oval, -90, 89, false, mPaint);

         */

        float radius;

        if (mScreenX > mScreenY) {
            radius = mScreenY / 4;
        } else {
            radius = mScreenX / 4;
        }

        Path path = new Path();
        path.addCircle(mScreenX / 2,
                mScreenY / 2, radius,
                Path.Direction.CW);

        //mPaint.setColor(Color.WHITE);
        //mPaint.setStrokeWidth(5);
        //mPaint.setStyle(Paint.Style.FILL);

        float center_x, center_y;
        final RectF oval = new RectF();
        //mPaint.setStyle(Paint.Style.STROKE);

        center_x = mScreenX / 2;
        center_y = mScreenY / 2;

        oval.set(center_x - radius,
                center_y - radius,
                center_x + radius,
                center_y + radius);
        mCanvas.drawArc(oval, 90, 180, false, mPaint);

    }

    /**
     * draw a filled arrow from (x0, y0) to (x1, y1)
     * @param canvas
     * @param mPaint
     * @param x0
     * @param y0
     * @param x1
     * @param y1
     */
    public static void fillArrow(Canvas canvas, Paint mPaint, float x0, float y0, float x1, float y1) {

        mPaint.setStyle(Paint.Style.FILL);

        float deltaX = x1 - x0;
        float deltaY = y1 - y0;
        float frac = (float) 0.1;

        float point_x_1 = x0 + (float) ((1 - frac) * deltaX + frac * deltaY);
        float point_y_1 = y0 + (float) ((1 - frac) * deltaY - frac * deltaX);

        float point_x_2 = x1;
        float point_y_2 = y1;

        float point_x_3 = x0 + (float) ((1 - frac) * deltaX - frac * deltaY);
        float point_y_3 = y0 + (float) ((1 - frac) * deltaY + frac * deltaX);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);

        path.moveTo(point_x_1, point_y_1);
        path.lineTo(point_x_2, point_y_2);
        path.lineTo(point_x_3, point_y_3);
        path.lineTo(point_x_1, point_y_1);
        path.lineTo(point_x_1, point_y_1);
        path.close();

        canvas.drawPath(path, mPaint);
    }

    /**
     * Calculate angle between two lines with two given points
     *
     * @param A1 First point first line
     * @param A2 Second point first line
     * @param B1 First point second line
     * @param B2 Second point second line
     * @return Angle between two lines in degrees
     */
    public static float angleBetween2Lines(PointF A1, PointF A2, PointF B1, PointF B2) {
        float angle1 = (float) Math.atan2(A2.y - A1.y, A1.x - A2.x);
        float angle2 = (float) Math.atan2(B2.y - B1.y, B1.x - B2.x);
        //float calculatedAngle = (float) Math.toDegrees(angle1 - angle2);
        //float calculatedAngle = (float) ((angle2 - angle1) * 180 / Math.PI);
        float calculatedAngle = (float) (angle2 - angle1);
        System.out.println("angle=" + calculatedAngle);
        if (calculatedAngle < 0) calculatedAngle = 0;   // += 360
        else if (calculatedAngle > Math.PI) calculatedAngle = (float) Math.PI;
        return calculatedAngle;
    }

    /**
     * this method shuffle the elements of the array
     * @param ar : the array to be shuffled
     */
    public static void shuffleArray(int[] ar)
    {
        // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    /**
     * Pick k numbers between 0 (inclusive) and n (inclusive)
     */
    public static ArrayList<Integer> pickRandom(int n, int k) {
        Random random = new Random();
        //Integer[] pickedList;
        final ArrayList<Integer> picked = new ArrayList<>();
        int size = k; //(n > k) ? n : k;

        while (picked.size() < size) {
            picked.add(random.nextInt(n + 1));
        }

        //pickedList = picked.toArray(new Integer[0]);
/*
        for (int i = k ; i < n ; i++) {
            pickedList[i] = random.nextInt(n + 1);
        }
*/
        return picked;
    }

    public static void rectFToRect(RectF rectF, Rect rect) {
        rect.left = Math.round(rectF.left);
        rect.top = Math.round(rectF.top);
        rect.right = Math.round(rectF.right);
        rect.bottom = Math.round(rectF.bottom);
    }//from w  w w . j  a v a 2 s  . c o m

    public static Rect rectFToRect(RectF rectF) {
        Rect rect = new Rect();
        rectFToRect(rectF, rect);
        return rect;
    }

    public static Bitmap rotateBitmap(Bitmap original, float degrees) {
        int width = original.getWidth();
        int height = original.getHeight();

        Matrix matrix = new Matrix();
        matrix.preRotate(degrees);

        Bitmap rotatedBitmap = Bitmap.createBitmap(original, 0, 0, width, height, matrix, true);
        //Canvas canvas = new Canvas(rotatedBitmap);
        //canvas.drawBitmap(original, 5.0f, 0.0f, null);

        return rotatedBitmap;
    }

    public static Bitmap resizeImage(Bitmap realImage, float maxImageSize, boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /*
    private void drawFoggyWindowWithTransparentCircle(Canvas canvas,
                                                      float circleX, float circleY, float radius) {

        //Get the "foggy window" bitmap
        BitmapDrawable foggyWindow =
                (BitmapDrawable) getResources().getDrawable(R.drawable.ic_launcher_background);//foggy_window);
        Bitmap foggyWindowBmp = foggyWindow.getBitmap();

        //Create a temporary bitmap
        Bitmap tempBitmap = Bitmap.createBitmap(
                foggyWindowBmp.getWidth(),
                foggyWindowBmp.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas tempCanvas = new Canvas(tempBitmap);

        //Copy foggyWindowBmp into tempBitmap
        tempCanvas.drawBitmap(foggyWindowBmp, 0, 0, null);

        //Create a radial gradient
        RadialGradient gradient = new android.graphics.RadialGradient(
                circleX, circleY,
                radius, 0xFF000000, 0x00000000,
                android.graphics.Shader.TileMode.CLAMP);

        //Draw transparent circle into tempBitmap
        Paint p = new Paint();
        p.setShader(gradient);
        p.setColor(0xFF000000);
        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        tempCanvas.drawCircle(circleX, circleY, radius, p);

        //Draw tempBitmap onto the screen (over what's already there)
        canvas.drawBitmap(tempBitmap, 0, 0, null);
    }
    */
}
