package com.example.sandwichhunter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sandwichhunter.firebase.AvailableGame;
import com.example.sandwichhunter.firebase.MultiplayerGameManager;
import com.example.sandwichhunter.firebase.MultiplayerPlayer;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class JoinActivity extends AppCompatActivity {

    private Button backButton;
    //private ArrayList<AvailableGame> availableGames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);

        backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JoinActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        MultiplayerGameManager.getAvailableGames(this); // Will trigger displayAvailableGames

    }

    public void displayAvailableGames(ArrayList<AvailableGame> availableGames) {

        //this.availableGames = availableGames;

        List<String> availableGamesStrings = new ArrayList<>(availableGames.size());
        for (AvailableGame availableGame : availableGames) {
            availableGamesStrings.add(Objects.toString(availableGame, null));
        }

        ListView listView = (ListView) findViewById(R.id.listview);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, availableGamesStrings);

        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                for (AvailableGame availableGame : availableGames) {
                    if (selectedItem.equals(availableGame.toString())) {
                        joinGame(availableGame.getGameHash());
                    }
                }
            }
        });
    }

    private void joinGame(String gameHash) {
        MultiplayerGameManager.joinGame(gameHash);
        joinLobby();
    }

    private void joinLobby() {

        Intent intent = new Intent(JoinActivity.this, LobbyActivity.class);
        startActivity(intent);
    }

}