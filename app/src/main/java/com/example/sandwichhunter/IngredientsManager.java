package com.example.sandwichhunter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.sandwichhunter.firebase.MultiplayerGameManager;
import com.example.sandwichhunter.firebase.MultiplayerPlayer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.SplittableRandom;

public class IngredientsManager implements PropertyChangeListener {
    // to generate the X-position of the falling ingredients randomly
    private Random positionGenerator;

    // SplittableRandom : more adequate for probabilities

    // to select randomly whether the ingredient is rotten or not
    SplittableRandom rottenGenerator;
    // to select randomly the id of the ingredient i.e its type
    SplittableRandom idGenerator;

    // stock the last time we generated an ingredient
    private long lastFrameTime;

    // the number of type of the ingredients
    public static final int MAX_ING = 8;

    // the current ingredients in the screen (the ones falling)
    private ArrayList<Ingredient> currentIngredients;

    // ingredients colors
    public static final int[][] ingredientsColors = new int[][] {
            {255, 0, 0},    // red
            {0, 255, 0},    // green
            {0, 0, 255},    // blue
            {255, 255, 0},  // yellow
            {255, 0, 255},
            {0, 255, 255},
            {100, 100, 255},
            {255, 100, 100},
            {100, 255, 100},
            {50, 75, 100},
            {255, 255, 255}
    };

    // the screen's dimensions
    private int mScreenX, mScreenY;

    // the maximum Y-position the falling ingredient should not got past
    private float limitY;

    // the max and min X-position defining the range following the X-axis
    // where the ingredients can be generated randomly
    private int maxPosX, minPosX;

    public static Bitmap[] ingredientsFrames;
    public static Bitmap[] rottenIngredientsFrames;

    /*
    RequiresApi : necessary for SplittableRandom
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public IngredientsManager(int x, int y, float limitY) {
        positionGenerator = new Random();
        rottenGenerator = new SplittableRandom();
        idGenerator = new SplittableRandom();

        // Capture the current time in milliseconds
        lastFrameTime = System.currentTimeMillis();

        currentIngredients = new ArrayList<>();

        mScreenX = x;
        mScreenY = y;

        maxPosX = 7*mScreenX/8;
        minPosX = mScreenX/8;

        this.limitY = limitY;

        //BitmapFactory bf = new BitmapFactory();
/*
        ingredientsFrames = new Bitmap[] {
                Tools.decodeSampledBitmapFromResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.avocado, mScreenX/10, mScreenX/10),
                Tools.decodeSampledBitmapFromResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.bread, mScreenX/10, mScreenX/10),
                Tools.decodeSampledBitmapFromResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.cheese, mScreenX/10, mScreenX/10),
                Tools.decodeSampledBitmapFromResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.fried_egg, mScreenX/10, mScreenX/10),
                Tools.decodeSampledBitmapFromResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.ham, mScreenX/10, mScreenX/10),
                Tools.decodeSampledBitmapFromResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.onion, mScreenX/10, mScreenX/10),
                Tools.decodeSampledBitmapFromResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.salad, mScreenX/10, mScreenX/10),
                Tools.decodeSampledBitmapFromResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.tomato, mScreenX/10, mScreenX/10) };
        */

        /*ingredientsFrames = new Bitmap[] {
            bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.avocado),
            bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.bread),
            bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.cheese),
            bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.fried_egg),
            bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.ham),
            bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.onion),
            bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.salad),
            bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.tomato) };*/

        ingredientsFrames = getIngredientsFrames();
        rottenIngredientsFrames = getRottenIngredientsFrames();

/*
        ingredientsFrames[0] = Tools.resizeImage(ingredientsFrames[0], mScreenX/20, false);
        ingredientsFrames[1] = Tools.resizeImage(ingredientsFrames[1], mScreenX/20, false);
        ingredientsFrames[2] = Tools.resizeImage(ingredientsFrames[2], mScreenX/20, false);
        ingredientsFrames[3] = Tools.resizeImage(ingredientsFrames[3], mScreenX/20, false);
        ingredientsFrames[4] = Tools.resizeImage(ingredientsFrames[4], mScreenX/20, false);
        ingredientsFrames[5] = Tools.resizeImage(ingredientsFrames[5], mScreenX/20, false);
        ingredientsFrames[6] = Tools.resizeImage(ingredientsFrames[6], mScreenX/20, false);
        ingredientsFrames[7] = Tools.resizeImage(ingredientsFrames[7], mScreenX/20, false);
        */

        /*rottenIngredientsFrames = new Bitmap[]{
        bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.shoe),
        bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.rat) };*/
        /*
        rottenIngredientsFrames[0] = Tools.resizeImage(rottenIngredientsFrames[0], mScreenX/20, false);
        rottenIngredientsFrames[1] = Tools.resizeImage(rottenIngredientsFrames[1], mScreenX/20, false);
        */

        // Subscribe to changes to ingredient in MultiplayerGameManager
        this.addPropertyChangeListeners();
    }

    private Bitmap[] getIngredientsFrames() {
        BitmapFactory bf = new BitmapFactory();

        Bitmap[] ingredientsFrames = new Bitmap[] {
                bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.avocado),
                bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.bread),
                bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.cheese),
                bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.fried_egg),
                bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.ham),
                bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.onion),
                bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.salad),
                bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.tomato) };

        return ingredientsFrames;
    }

    private Bitmap[] getRottenIngredientsFrames() {
        BitmapFactory bf = new BitmapFactory();

        Bitmap[] rottenIngredientsFrames = new Bitmap[]{
                bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.shoe),
                bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.rat) };

        return rottenIngredientsFrames;
    }

    public ArrayList<Ingredient> getCurrentIngredients() { return currentIngredients; }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void update(long fps) {
        // update every ingredient in the currentIngerdients list
        for (int j = 0 ; j < currentIngredients.size() ; j++) {
            if (currentIngredients.get(j).update(fps, limitY)) {
                currentIngredients.remove(j);
                j--;
            }
        }

        // Capture the current time in milliseconds
        long currentFrameTime = System.currentTimeMillis();

        // generate a new ingredient every 1 sec if player is host
        if ((currentFrameTime - lastFrameTime) >= 1000) {

            if (MultiplayerGameManager.playerIsHost()) {
                generateIngredients();
            }

            // stock the current time to remember the last time we generated an ingredient
            lastFrameTime = currentFrameTime;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void generateIngredients() {
        // int random_pos = (int)Math.floor(Math.random()*(max-min+1)+min);
        // select randomly a X-pos in [min, max]
        int random_pos = positionGenerator.nextInt(maxPosX - minPosX) + minPosX;

        // the ingredient will be rotten with a probability of 20%
        boolean isRotten = rottenGenerator.nextInt(100) < 20;

        // MAX_ING is reserved for rotten ingredients
        int ingredientId = MAX_ING;

        //Bitmap frame;

        // if not rotten select an 0 <= ID < MAX_ING
        if (!isRotten) {
            ingredientId = idGenerator.nextInt(MAX_ING);
            //System.out.println("ID: "+ ingredientId);
            //frame = ingredientsFrames[ingredientId];
        }
        /*else {
            frame = rottenIngredientsFrames[0];
        }*/

        // Add ingredient to database
        //System.out.println("set value IM: " + isRotten);
        MultiplayerGameManager.setIngredient(ingredientId, random_pos, mScreenX, mScreenY, isRotten);

        // generate the new ingredient and add it to the currentIngredients list
        //currentIngredients.add(new Ingredient(ingredientId, random_pos, mScreenX, mScreenY, isRotten, frame));
    }

    /**
     * draws the falling ingredients
     * @param mCanvas
     * @param mPaint
     * @param argb : the color
     */
    public void draw(Canvas mCanvas, Paint mPaint, int argb) {
        // draw every ingredient in the currentIngredients list

        try {
            for (Ingredient ingredient : currentIngredients) {
                ingredient.draw(mCanvas);
            /*
            mPaint.setColor(Color.argb(255, ingredientsColors[ingredient.getId()][0], ingredientsColors[ingredient.getId()][1], ingredientsColors[ingredient.getId()][2]));
            mCanvas.drawRect(ingredient.getRect(), mPaint);
             */
            }
        } catch (Exception e) {
            System.out.println("fuck");
            System.out.println(e);
        }

    }

    /**
     * called when a shot collides with the ingredient currentIngredients[index]
     * this ingredient should be romoved from the currentIngredients list
     * @param index : the ingredient index in the list
     */
    public void ingredientExploded(int index) {
        if (index < currentIngredients.size()) {
            currentIngredients.remove(index);
        }
    }

    /**
     * called when an ingredient go past the Y-limit or shot by someone else
     * @param index : the ingredient index in the list
     */
    public void removeIng(int index) {
        currentIngredients.remove(index);
    }

    private void addPropertyChangeListeners() {
        MultiplayerGameManager.addPropertyChangeListener(this);
    }

    private void removePropertyChangeListeners() {
        MultiplayerGameManager.removePropertyChangeListener(this);
    }

    public void propertyChange(PropertyChangeEvent evt) {

        if (evt.getNewValue() instanceof HashMap) {
            if ("ingredient".equals(evt.getPropertyName())) {
                this.addIngredient((HashMap<String, Long>) evt.getNewValue());
            }
            return;
        }

        if (evt.getNewValue() instanceof Long) {
            if ("shotIngredient".equals(evt.getPropertyName())) {
                int index = this.getIngredientIndex((long) evt.getNewValue());
                if (index >= 0) {
                    this.removeIng(index);
                }
            }
            return;
        }
    }

    private void addIngredient(HashMap<String, Long> ingredient) {
        int ingredientID = ingredient.get("ingredientID").intValue();
        long ingredientHash = ingredient.get("ingredientHash");
        int position = ingredient.get("position").intValue();
        int screenWidth = ingredient.get("screenWidth").intValue();
        int screenHeight = ingredient.get("screenHeight").intValue();
        boolean isRotten = ingredient.get("isRotten") == 1 ? true : false;

        Bitmap frame = null;
        if (isRotten) {
            Random r = new Random();
            frame = rottenIngredientsFrames[r.nextInt(2)];
        } else if (ingredientID < ingredientsFrames.length) {
            frame = ingredientsFrames[ingredientID];
        }

        if (frame != null) {
            //add new ingredient to the currentIngredients list
            currentIngredients.add(new Ingredient(ingredientID, ingredientHash, position, screenWidth, screenHeight, isRotten, frame));
        }
    }

    private int getIngredientIndex(long ingredientHash) {
        for (int i = 0 ; i < currentIngredients.size(); i++) {
            if (currentIngredients.get(i).getIngredientHash() == ingredientHash) {
                return i;
            }
        }
        return -1;
    }
}
