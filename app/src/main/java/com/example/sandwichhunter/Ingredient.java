package com.example.sandwichhunter;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.RectF;

import java.util.ArrayList;

public class Ingredient {
    private int screenWidth, screenHeight;
    private float mIngredientWidth, mIngredientHeight;

    // the Rectangle where we will draw the ingredient
    private RectF mRect;

    // the ingredient velocity
    private float mVelocity;

    // whether is rotten or not
    private boolean rotten;

    // its ID
    private int id;

    private long ingredientHash = 0;

    // to check whether the shot collided with other ingredients or not
    private boolean exploded;

    private Animation animation;

    /**
     *
     * @param id : the ingredient's type
     * @param startX : the X-position where the ingredient will be generated
     * @param screenX
     * @param screenY
     * @param r : rotten or not
     */
    public Ingredient(int id, float startX, int screenX, int screenY, boolean r){
        screenWidth = screenX;
        screenHeight = screenY;

        // Make the mBall size relative to the screen resolution
        mIngredientWidth = screenX / 15;
        mIngredientHeight = mIngredientWidth;

        mVelocity = screenY/15;

        // Initialize the Rect that represents the Ingredient
        mRect = new RectF(startX - mIngredientWidth/2, 0, startX + mIngredientWidth/2, mIngredientHeight);

        exploded = false;
        rotten = r;
        this.id = id;
    }

    /**
     *
     * @param id : the ingredient's type
     * @param startX : the X-position where the ingredient will be generated
     * @param screenX
     * @param screenY
     * @param r : rotten or not
     */
    public Ingredient(int id, long ingredientHash, float startX, int screenX, int screenY, boolean r, Bitmap frame){
        screenWidth = screenX;
        screenHeight = screenY;

        this.ingredientHash = ingredientHash;

        // Make the mBall size relative to the screen resolution
        mIngredientWidth = screenX / 10;
        mIngredientHeight = mIngredientWidth;

        mVelocity = screenY/10;

        // Initialize the Rect that represents the Ingredient
        mRect = new RectF(startX - mIngredientWidth/2, 0, startX + mIngredientWidth/2, mIngredientHeight);

        exploded = false;
        rotten = r;
        this.id = id;

        animation = new Animation(frame);
        animation.play();
    }

    public long getIngredientHash() {
        return this.ingredientHash;
    }

    // Give access to the Rect
    public RectF getRect() { return mRect; }

    public float getmIngredientHeight() { return mIngredientHeight; }

    public float getmIngredientWidth() { return mIngredientWidth; }

    public boolean isRotten() { return rotten; }

    public int getId() { return id; }

    public Animation getAnimation() { return animation; }

    // Change the position each frame
    public boolean update(long fps, float limitY) {
        mRect.top = mRect.top + (mVelocity / fps);
        mRect.bottom = mRect.top + mIngredientHeight;

        // if the ingredient leaves the screen
        if (mRect.bottom >= limitY) {
            animation.stop();
            return true;
        }

        animation.update();

        return false;
    }

    public void draw(Canvas mCanvas) {
        animation.draw(mCanvas, Tools.rectFToRect(mRect));
    }

    public void explode() {
        exploded = true;
    }

}
