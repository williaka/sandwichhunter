package com.example.sandwichhunter;

import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.SurfaceView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.sandwichhunter.firebase.FirebaseManager;
import com.example.sandwichhunter.firebase.MultiplayerGameManager;

import java.util.Random;

public class StartGame extends AppCompatActivity {
    private GameView gameView;
    private MediaPlayer mediaPlayer;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Start the game in database if host
        if (MultiplayerGameManager.playerIsHost()) {
            MultiplayerGameManager.updateStatus("started");
        }

        // Get a Display object to access screen details
        Display display = getWindowManager().getDefaultDisplay();

        // Load the resolution into a Point object
        Point size = new Point();
        display.getSize(size);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Constants.SCREEN_WIDTH = dm.widthPixels;
        Constants.SCREEN_HEIGHT = dm.heightPixels;

        mediaPlayer = MediaPlayer.create(StartGame.this, R.raw.song);
        mediaPlayer.setLooping(true);
        //mediaPlayer.setVolume(100, 100);
        mediaPlayer.start();

        // Initialize pongView and set it as the view
        gameView = new GameView(this, size.x, size.y);

        setContentView((SurfaceView)gameView);

    }

    public void endGame() {
        MultiplayerGameManager.updateStatus("ended");

        Intent intent = new Intent(StartGame.this, PostgameActivity.class);
        startActivity(intent);
    }


    // This method executes when the player starts the game
    @Override
    protected void onResume() {
        super.onResume();

        // Tell the pongView resume method to execute
        gameView.resume();
        mediaPlayer.start();
    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();

        // Tell the pongView pause method to execute
        gameView.pause();
        mediaPlayer.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        mediaPlayer.stop();
        mediaPlayer.release();
    }
}
