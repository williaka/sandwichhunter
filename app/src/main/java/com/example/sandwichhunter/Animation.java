package com.example.sandwichhunter;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Animation {
    private Bitmap frame;
    private boolean isPlaying = false;
    //private float frameTime;
    //private long lastFrame;

    public Animation(Bitmap frame, float animTime) {
        this.frame = frame;

        //frameTime = animTime;

        //lastFrame = System.currentTimeMillis();
    }

    public Animation(Bitmap frame) {
        this.frame = frame;

        //frameTime = animTime;

        //lastFrame = System.currentTimeMillis();
    }

    public void setFrame(Bitmap frame) {
        this.frame = frame;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void play() {
        isPlaying = true;
        //lastFrame = System.currentTimeMillis();
    }
    public void stop() {
        isPlaying = false;
    }

    public void draw(Canvas canvas, Rect destination) {
        if(!isPlaying)
            return;

        scaleRect(destination);

        canvas.drawBitmap(frame, null, destination, new Paint());
    }

    private void scaleRect(Rect rect) {
        float whRatio = (float)(frame.getWidth())/frame.getHeight();
        if(rect.width() > rect.height())
            rect.left = rect.right - (int)(rect.height() * whRatio);
        else
            rect.top = rect.bottom - (int)(rect.width() * (1/whRatio));
    }

    public void update() {
        if(!isPlaying)
            return;
/*
        if(System.currentTimeMillis() - lastFrame > frameTime*1000) {
            lastFrame = System.currentTimeMillis();
        }
        */
    }
}
