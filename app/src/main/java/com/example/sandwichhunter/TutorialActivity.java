package com.example.sandwichhunter;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.SplittableRandom;

public class TutorialActivity extends AppCompatActivity {

    private Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TutorialActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });



        //System.out.println(MultiplayerGameManager.getGameHashes());

        //testClass t = (testClass) getIntent().getSerializableExtra("info");

        /*MultiplayerGameManager multiplayerGameManager = (MultiplayerGameManager) getIntent().getSerializableExtra("multiplayerGameManager");

        System.out.println(multiplayerGameManager.getGameHash());*/

        //gene();

    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void gene() {
        SplittableRandom SR = new SplittableRandom();


        for (int k = 0; k < 256; k++) {
            int i = SR.nextInt(8);
            System.out.println("ID: " + i);
            if (i >= 8 ) {
                System.out.println("attention!");
            }
        }

    }
}