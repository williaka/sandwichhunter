package com.example.sandwichhunter.firebase;

import android.graphics.Bitmap;

import com.example.sandwichhunter.Ingredient;
import com.example.sandwichhunter.IngredientsManager;
import com.example.sandwichhunter.JoinActivity;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class MultiplayerGameManager {

    // TODO: Add a method to increase own player score from main game

    private static FirebaseManager firebaseManager;
    private static MultiplayerPlayer player;
    private static String gameHash;

    private static ArrayList<MultiplayerPlayer> players = new ArrayList<MultiplayerPlayer>();
    private static HashMap<String, Long> ingredient;
    private static long shotIngredient;
    private static String status;
    private static String host;

    private static PropertyChangeSupport support = new PropertyChangeSupport(MultiplayerPlayer.class);

    public static void addPropertyChangeListener(PropertyChangeListener pcl) {
        MultiplayerGameManager.support.addPropertyChangeListener(pcl);
    }

    public static void removePropertyChangeListener(PropertyChangeListener pcl) {
        MultiplayerGameManager.support.removePropertyChangeListener(pcl);
    }

    public static void setFirebaseManager(MultiplayerPlayer player) {
        MultiplayerGameManager.firebaseManager = new FirebaseManager();
        MultiplayerGameManager.player = player;
    }

    private static void unsetFirebaseManager() {
        MultiplayerGameManager.firebaseManager = null;
        MultiplayerGameManager.player = null;
        MultiplayerGameManager.gameHash = null;

        MultiplayerGameManager.players = new ArrayList<MultiplayerPlayer>();
        MultiplayerGameManager.ingredient = null;
        MultiplayerGameManager.status = null;
        MultiplayerGameManager.host = null;
    }

    // Creates a new game in the database, and adds the current player to it
    public static void createGameInstanceInFirebase() {
        if (firebaseManager != null) {
            MultiplayerGameManager.gameHash = generateHash(64, true);
            MultiplayerGameManager.host = MultiplayerGameManager.player.getUsername();
            MultiplayerGameManager.firebaseManager.createGame();
            MultiplayerGameManager.firebaseManager.addPlayerToGame();
            MultiplayerGameManager.subscribeToGameChanges();
        }
    }

    public static void removeGameInstanceInFirebase() {
        if (firebaseManager != null) {
            MultiplayerGameManager.firebaseManager.removeGame();
        }
    }

    protected static void closeGame() {
        String host = MultiplayerGameManager.host;
        MultiplayerGameManager.unsubscribeToGameChanges();
        MultiplayerGameManager.unsetFirebaseManager();
        MultiplayerGameManager.support.firePropertyChange("host", host, MultiplayerGameManager.host);
    }

    // Joins an existing game instance
    public static void joinGame(String gameHash) {
        if (firebaseManager != null) {
            MultiplayerGameManager.gameHash = gameHash;
            MultiplayerGameManager.firebaseManager.addPlayerToGame();
            MultiplayerGameManager.subscribeToGameChanges();
        }
    }

    // Leave an existing game instance
    public static void leaveGame() {
        if (firebaseManager != null) {
            MultiplayerGameManager.unsubscribeToGameChanges();
            MultiplayerGameManager.firebaseManager.removePlayerFromGame(MultiplayerGameManager.getPlayer());
            MultiplayerGameManager.gameHash = null;
        }
    }

    // Change the ingredient in the database
    public static void setIngredient(long ingredientID, long position, long screenWidth, long screenHeight, boolean isRotten) {
        //System.out.println("set value MGM: " + isRotten);
        long ingredientHash = Long.parseLong(MultiplayerGameManager.generateHash(16, false), 10);
        if (firebaseManager != null) {
            MultiplayerGameManager.firebaseManager.spawnIngredient(ingredientID, ingredientHash, position, screenWidth, screenHeight, isRotten);
        }
    }

    public static boolean playerIsHost() {
        if (MultiplayerGameManager.getHost() == null) {
            return false;
        }
        return MultiplayerGameManager.getHost().equals(MultiplayerGameManager.getPlayer().getUsername());
    }

    public static void getAvailableGames(JoinActivity joinActivity) {
        if (firebaseManager != null) {
            firebaseManager.getAvailableGames(joinActivity);
        }
    }

    public static ArrayList<MultiplayerPlayer> getPlayers() {
        return MultiplayerGameManager.players;
    }

    protected static void returnAvailableGames(ArrayList<AvailableGame> availableGames, JoinActivity joinActivity) {
        joinActivity.displayAvailableGames(availableGames);
    }

    // Subscribe to changes in the realtime database
    private static void subscribeToGameChanges() {
        if (firebaseManager != null) {
            MultiplayerGameManager.firebaseManager.setOnValueChangedListeners();
        }
    }

    // Unsubscribe to changes in the realtime database
    private static void unsubscribeToGameChanges() {

        if (firebaseManager != null) {
            MultiplayerGameManager.firebaseManager.removeOnValueChangedListeners();
        }
    }

    public static MultiplayerPlayer getPlayer() {
        return MultiplayerGameManager.player;
    }

    public static String getGameHash() {
        return MultiplayerGameManager.gameHash;
    }

    private static String generateHash(int hashLength, boolean alphaNumeric) {

        String chars = "0123456789";
        if (alphaNumeric) {
            chars += "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        }

        Random random = new Random();

        StringBuilder hash = new StringBuilder();
        for(int i = 0; i < hashLength; i++) {
            hash.append(chars.charAt(random.nextInt(chars.length())));
        }

        return hash.toString();
    }

    public static String getStatus() {
        return MultiplayerGameManager.status;
    }

    public static void updateStatus(String status) {
        if (firebaseManager != null) {
            MultiplayerGameManager.firebaseManager.setStatus(status);
        }
    }

    public static void updateShotIngredient(long ingredientHash) {
        if (firebaseManager != null) {
            MultiplayerGameManager.firebaseManager.shootIngredient(ingredientHash);
        }
    }

    public static void updatePlayerScore(long score) {
        if (firebaseManager != null) {
            MultiplayerGameManager.firebaseManager.setScore(score);
        }
    }

    public static String getHost() {
        return MultiplayerGameManager.host;
    }

    // Takes in strings from database with subscriptions
    protected static void setString(String string, String value) {
        switch(string) {
            case "status":
                MultiplayerGameManager.setStatus(value);
                break;
            case "host":
                MultiplayerGameManager.setHost(value);
                break;
            /*case "spawned_item":
                MultiplayerGameManager.setSpawnedItem(value);
                break;*/
            default:
                // Do nothing, string not found
        }
    }

    // Takes in longs from database with subscriptions
    protected static void setLong(String string, Long value) {
        switch(string) {
            case "shotIngredient":
                MultiplayerGameManager.setShotIngredient(value);
                break;
            default:
                // Do nothing, string not found
        }
    }

    // Takes in hashmap from database with subscriptions
    protected static void setHashMap(String string, HashMap<String, Long> map) {
        switch(string) {
            case "players":
                MultiplayerGameManager.updatePlayersAndScores(map);
                break;
            case "ingredient":
                MultiplayerGameManager.updateIngredient(map);
                break;

            default:
                // Do nothing, hashmap not found
        }
    }

    private static void setShotIngredient(long ingredientHash) {
        MultiplayerGameManager.support.firePropertyChange("shotIngredient", MultiplayerGameManager.shotIngredient, ingredientHash);
        MultiplayerGameManager.shotIngredient = ingredientHash;
    }

    private static void setStatus(String status) {
        System.out.println("status: " + status);
        MultiplayerGameManager.support.firePropertyChange("status", MultiplayerGameManager.status, status);
        MultiplayerGameManager.status = status;
    }

    private static void setHost(String host) {
        System.out.println("the host is: " + host);
        MultiplayerGameManager.host = host;
    }

    private static void updateIngredient(HashMap<String, Long> ingredient) {

        MultiplayerGameManager.support.firePropertyChange("ingredient", MultiplayerGameManager.ingredient, ingredient);
        MultiplayerGameManager.ingredient = ingredient;

    }

    private static void updatePlayersAndScores(HashMap<String, Long> playerData) {

        // Add new players
        ArrayList<MultiplayerPlayer> addedPlayers = new ArrayList<MultiplayerPlayer>();
        for (String username: playerData.keySet()) {
            boolean found = false;
            for (MultiplayerPlayer player: MultiplayerGameManager.players) {
                if  (username.equals(player.getUsername())) {
                    player.setScore(playerData.get(username));
                    MultiplayerGameManager.support.firePropertyChange("score", 0, player.getScore());
                    found = true;
                    break;
                }
            }
            if (!found) {
                MultiplayerPlayer p = new MultiplayerPlayer(username);
                p.setScore(playerData.get(username));
                MultiplayerGameManager.support.firePropertyChange("score", 0, player.getScore());
                addedPlayers.add(p);
            }
        }
        addPlayers(addedPlayers);

        // Remove players
        ArrayList<MultiplayerPlayer> removedPlayers = new ArrayList<MultiplayerPlayer>();
        for (MultiplayerPlayer player: MultiplayerGameManager.players) {
            boolean found = false;
            for (String username: playerData.keySet()) {
                if (username.equals(player.getUsername())) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                removedPlayers.add(player);
            }
        }
        removePlayers(removedPlayers);
    }

    private static void addPlayers(ArrayList<MultiplayerPlayer> players) {
        ArrayList<MultiplayerPlayer> previousPlayers = new ArrayList<MultiplayerPlayer>(MultiplayerGameManager.players);
        MultiplayerGameManager.players.addAll(players);
        MultiplayerGameManager.support.firePropertyChange("players", previousPlayers, MultiplayerGameManager.players);
    }

    private static void removePlayers(ArrayList<MultiplayerPlayer> players) {
        ArrayList<MultiplayerPlayer> previousPlayers = new ArrayList<MultiplayerPlayer>(MultiplayerGameManager.players);
        MultiplayerGameManager.players.removeAll(players);

        MultiplayerGameManager.support.firePropertyChange("players", previousPlayers, MultiplayerGameManager.players);
    }
}
