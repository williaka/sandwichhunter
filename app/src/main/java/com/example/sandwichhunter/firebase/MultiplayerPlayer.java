package com.example.sandwichhunter.firebase;

public class MultiplayerPlayer {
    private String username;
    private long score = 0;

    public MultiplayerPlayer(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public long getScore() {
        return this.score;
    }

    protected void setScore(long points) {
        this.score = points;
    }
    /*

    protected void increaseScore(long points) {
        this.score += points;
    }





    public String toString() {
        return this.username +", Score: " + Long.toString(this.score);
    }*/

}
