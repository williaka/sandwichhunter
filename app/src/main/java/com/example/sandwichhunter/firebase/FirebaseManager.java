package com.example.sandwichhunter.firebase;

import com.example.sandwichhunter.JoinActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class FirebaseManager {

    private final FirebaseDatabase database;
    private final DatabaseReference rootReference;
    private DatabaseReference gameReference;

    private HashMap<String, ValueEventListener> valueEventListeners = new HashMap<String, ValueEventListener>();

    protected FirebaseManager() {
        this.database = FirebaseDatabase.getInstance();
        this.rootReference = database.getReference();
    }

    protected void createGame() {

        String gameHash = MultiplayerGameManager.getGameHash();
        this.gameReference = database.getReference(gameHash);

        this.gameReference.child("status").setValue("pre-lobby");

        String hostUsername = MultiplayerGameManager.getPlayer().getUsername();
        this.gameReference.child("host").setValue(hostUsername);
        this.spawnIngredient(0, 0, 0, 0, 0, false);
        this.shootIngredient(0);
    }

    protected void removeGame() {
        if (this.gameReference != null) {
            this.gameReference.removeValue(); // Remember to run on game end
        }
    }

    protected void addPlayerToGame() {

        this.gameReference = database.getReference(MultiplayerGameManager.getGameHash());

        String username = MultiplayerGameManager.getPlayer().getUsername();
        long userScore = MultiplayerGameManager.getPlayer().getScore();
        this.gameReference.child("players").child(username).setValue(userScore);
    }

    protected void removePlayerFromGame(MultiplayerPlayer multiplayerPlayer) {

        // TODO: Consider case of removed player == current player
        if (this.gameReference != null) {
            String username = multiplayerPlayer.getUsername();
            this.gameReference.child("players").child(username).removeValue();
        }
    }

    protected void setOnValueChangedListeners() {

        if (this.gameReference == null) {
            return;
        }

        String[] subscriptions = {"status", "host", "players", "ingredient", "shotIngredient"}; //spawned_item

        for (String subscription : subscriptions) {
            SetOnValueChangedListener(subscription);
        }
    }

    private void SetOnValueChangedListener(String subscription) {

        DatabaseReference valueReference = this.gameReference.child(subscription);

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                if (snapshot.getValue() instanceof String) {
                    MultiplayerGameManager.setString(subscription, (String) snapshot.getValue());
                } else if (snapshot.getValue() instanceof Long) {
                    MultiplayerGameManager.setLong(subscription, (Long) snapshot.getValue());
                } else if (snapshot.getValue() instanceof HashMap) {
                    MultiplayerGameManager.setHashMap(subscription, (HashMap<String, Long>) snapshot.getValue());
                } else {
                    System.out.println("Something unexpected changed for subscription: " + subscription);
                    if (snapshot.getValue() == null) {
                        MultiplayerGameManager.closeGame();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        };

        this.valueEventListeners.put(subscription, valueEventListener);
        valueReference.addValueEventListener(valueEventListener);
    }

    protected void removeOnValueChangedListeners() {

        for (String subscription: this.valueEventListeners.keySet()) {
            DatabaseReference valueReference = this.gameReference.child(subscription);
            ValueEventListener valueEventListener = this.valueEventListeners.get(subscription);
            if (valueEventListener != null) {
                valueReference.removeEventListener(valueEventListener);
            }
        }
    }

    protected void spawnIngredient(long ingredientID, long ingredientHash, long position, long screenWidth, long screenHeight, boolean isRotten) {

        HashMap<String, Long> ingredient = new HashMap<String, Long>();
        ingredient.put("ingredientID", ingredientID);
        ingredient.put("ingredientHash", ingredientHash);
        ingredient.put("position", position);
        ingredient.put("screenWidth", screenWidth);
        ingredient.put("screenHeight", screenHeight);
        ingredient.put("isRotten", (long) (isRotten ? 1 : 0));

        this.gameReference.child("ingredient").setValue(ingredient);
    }

    protected void shootIngredient(long ingredientHash) {
        this.gameReference.child("shotIngredient").setValue(ingredientHash);
    }

    protected void setStatus(String status) {
        this.gameReference.child("status").setValue(status);
    }

    protected void setScore(long score) {
        this.gameReference.child("players").child(MultiplayerGameManager.getPlayer().getUsername()).setValue(score);
    }


    protected void getAvailableGames(JoinActivity joinActivity) {
        ArrayList<AvailableGame> availableGames = new ArrayList<AvailableGame>();

        this.rootReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                snapshotLoop:
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (snapshot.getValue() instanceof java.util.HashMap) {
                        try {

                            String status = ((HashMap<String, String>) snapshot.getValue()).get("status");
                            if (!"lobby".equals(status)) {
                                continue;
                            }

                            String gameHash = snapshot.getKey();
                            String host = ((HashMap<String, String>) snapshot.getValue()).get("host");
                            //int numberOfPlayers = ((HashMap<String, HashMap<String, Long>>) snapshot.getValue()).get("players").size();

                            Set<String> playerSet = ((HashMap<String, HashMap<String, Long>>) snapshot.getValue()).get("players").keySet();
                            String[] players = new String[playerSet.size()];

                            int index = 0;
                            for (String player : playerSet) {
                                if (player.equals(MultiplayerGameManager.getPlayer().getUsername())) {
                                    continue snapshotLoop;
                                }
                                players[index++] = player;
                            }

                            availableGames.add(new AvailableGame(host, gameHash, players));
                        }
                        catch (Exception e) {
                            System.out.println(e);
                            continue;
                        }
                    }
                }
                MultiplayerGameManager.returnAvailableGames(availableGames, joinActivity);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }
}
