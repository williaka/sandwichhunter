package com.example.sandwichhunter.firebase;

public class AvailableGame {

    private String host;
    private String gameHash;
    private String[] playernames;

    protected AvailableGame(String host, String gameHash, String[] playernames) {
        this.host = host;
        this.gameHash = gameHash;
        this.playernames = playernames;
    }

    public String getGameHash() {
        return this.gameHash;
    }

    protected String[] getPlayernames() {
        return this.playernames;
    }

    public String toString() {
        return "Game hosted by: " + this.host + ", " + this.playernames.length + " players in game";
        //gameHash; //host + " " + gameHash + " " + Integer.toString(numberOfPlayers);
    }
}
