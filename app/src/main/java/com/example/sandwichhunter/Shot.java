package com.example.sandwichhunter;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Random;

public class Shot {
    // the Rectangle where we draw the shot
    private RectF mRect;
    private float mShotWidth, mShotHeight;
    private int screenWidth, screenHeight;

    // the shot's velocity following the X-axis
    private float mXVelocity;
    // the shot's velocity following the Y-axis
    private float mYVelocity;

    // the angle between the shot's trajectory and the horizontal line
    // it defines the trajectory of the shpt
    private float angDirection;

    // to check whether the shot collided with other ingredients or not
    private boolean exploded;

    private Animation shotAnimation;

    /**
     *
     * @param startX : from where the shot was shot
     * @param startY : from where the shot was shot
     * @param angle
     * @param screenX
     * @param screenY
     */
    public Shot(float startX, float startY, float angle, int screenX, int screenY){
        screenWidth = screenX;
        screenHeight = screenY;

        // Make the mBall size relative to the screen resolution
        mShotWidth = screenX/60;
        mShotHeight = mShotWidth;

        mYVelocity = screenY;   // /6
        mXVelocity = mYVelocity;

        angDirection = angle;

        // Initialize the Rect that represents the mBall
        mRect = new RectF(startX - mShotWidth/2, startY - mShotHeight, startX + mShotWidth/2, startY + mShotHeight);

        exploded = false;
    }

    /**
     *
     * @param startX : from where the shot was shot
     * @param startY : from where the shot was shot
     * @param angle
     * @param screenX
     * @param screenY
     */
    public Shot(float startX, float startY, float angle, int screenX, int screenY, Bitmap frame){
        screenWidth = screenX;
        screenHeight = screenY;

        // Make the mBall size relative to the screen resolution
        mShotWidth = screenX/10;
        mShotHeight = screenY/10;

        mYVelocity = screenY;   // /6
        mXVelocity = mYVelocity;

        angDirection = angle;

        // Initialize the Rect that represents the mBall
        mRect = new RectF(startX - mShotWidth/2, startY - mShotHeight, startX + mShotWidth/2, startY + mShotHeight);

        exploded = false;

        //shotAnimation = new Animation(Tools.rotateBitmap(frame, (float)(90 - angDirection * 180 / Math.PI)));
        shotAnimation = new Animation(frame);
        shotAnimation.play();
    }

    // Give access to the Rect
    public RectF getRect() { return mRect; }

    public float getmShotHeight() { return mShotHeight; }

    public float getmShotWidth() { return mShotWidth; }

    /**
     * update the position of the shot each frame
     * @param fps :  frame per second
     * @return true if the shot left the screen
     */
    public boolean update(long fps) {

        mRect.top = (float) (mRect.top - (mYVelocity * Math.sin(angDirection) / fps));
        mRect.bottom = mRect.top + mShotHeight;

        mRect.left = (float) (mRect.left + (mXVelocity * Math.cos(angDirection) / fps));
        mRect.right = mRect.left + mShotWidth;

        // if the shot leaves the screen
        if (mRect.bottom <= 0
                || mRect.right <= 0
                || mRect.left >= screenWidth) {
            return true;
        }

        return false;
    }

    public void draw(Canvas mCanvas) {
        shotAnimation.draw(mCanvas, Tools.rectFToRect(mRect));
    }

    /**
     * whether in shot collides with an ingredient or not
     * @param ingredients : the list of the current ingredients
     * @return : true if the shot collides with an ingredient
     */
    public Ingredient collides(ArrayList<Ingredient> ingredients) {
        for (Ingredient ingredient : ingredients) {
            /*
            if (ingredient.getRect().bottom >= this.mRect.top
                && (ingredient.getRect().right >= this.mRect.left
                    || ingredient.getRect().left <= this.mRect.right)) {
            */
            if (RectF.intersects(this.getRect(), ingredient.getRect())) {
                ingredient.explode();
                this.explode();
                return ingredient;
            }
        }
        return null;
    }

    public void explode() {
        exploded = true;
    }

}
