package com.example.sandwichhunter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.sandwichhunter.firebase.AvailableGame;
import com.example.sandwichhunter.firebase.MultiplayerGameManager;
import com.example.sandwichhunter.firebase.MultiplayerPlayer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

public class PostgameActivity extends AppCompatActivity implements PropertyChangeListener {

    private Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postgame);

        this.backButton = findViewById(R.id.backButton);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToMenu();
            }
        });

        addPropertyChangeListeners();
        updateScoreTable();
    }

    private void goToMenu() {

        if (MultiplayerGameManager.playerIsHost()) {
            MultiplayerGameManager.removeGameInstanceInFirebase();
        } else {
            MultiplayerGameManager.leaveGame();
        }

        removePropertyChangeListeners();

        Intent intent = new Intent(PostgameActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void updateScoreTable() {

        System.out.println("Updating scores");

        ArrayList<MultiplayerPlayer> players = MultiplayerGameManager.getPlayers();

        List<String> results = new ArrayList<>(players.size());
        for (MultiplayerPlayer player : players) {
            results.add(Objects.toString(player.getUsername() + " scored: " + player.getScore(), ""));
        }

        for (String s : results) {
            System.out.println(s);
        }

        ListView listView = (ListView) findViewById(R.id.listview);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, results);
        listView.setAdapter(arrayAdapter);
    }

    private void addPropertyChangeListeners() {
        MultiplayerGameManager.addPropertyChangeListener(this);
    }

    private void removePropertyChangeListeners() {
        MultiplayerGameManager.removePropertyChangeListener(this);
    }

    public void propertyChange(PropertyChangeEvent evt) {

        if (evt.getNewValue() instanceof Long) {
            if ("score".equals(evt.getPropertyName())) {
                updateScoreTable();
            }
            return;
        }
    }
}