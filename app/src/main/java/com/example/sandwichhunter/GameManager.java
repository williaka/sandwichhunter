package com.example.sandwichhunter;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * this class allows to manage the game by
 * creating the recipes and handling the collision between shots and ingredients
 */
public class GameManager {
    // stock the ingredients dimensions
    private int ingredient_width, ingredient_height;

    // stock the screen dimensions
    private int mScreenX, mScreenY;

    // stock the player's recipe information

    // recipe length
    private int recipe_len_1;
    // the recipe : a static list of the IDs of the ingredients
    private Integer[] recipe_1;

    // an iterator that points to the next ingredient in the recipe
    // that hasn't been shot yet
    private int next_ing_1;

    ArrayList<Animation> recipeFrames_1;

    public GameManager(int mScreenX, int mScreenY, int recipe_len_1) {
        this.mScreenX = mScreenX;
        this.mScreenY = mScreenY;

        this.ingredient_width = mScreenX/20;
        this.ingredient_height = this.ingredient_width;

        this.recipe_len_1 = recipe_len_1;

        System.out.println(recipe_len_1);

        ArrayList<Integer> picked = Tools.pickRandom(IngredientsManager.MAX_ING - 1, recipe_len_1 - 2);

        System.out.println(picked.size());

        picked.add(0, 1);
        picked.add(1);

        this.recipe_1 = picked.toArray(new Integer[0]);

        // intuitively the first ingredient to shoot is the first one on the list
        this.next_ing_1 = 0;

        recipeFrames_1 = new ArrayList<Animation>(recipe_len_1);

        // we generate the recipe's ingredients randomly
        // using only their IDs
        Random random = new Random();
        for (int i = 0 ; i < recipe_len_1; i++) {
            //recipe_1[i] = random.nextInt(IngredientsManager.MAX_ING);
            recipeFrames_1.add(new Animation(IngredientsManager.ingredientsFrames[recipe_1[i]]));
            recipeFrames_1.get(i).play();
        }


    }

    /**
     *
     * @param playerId : 1 for the player1 and 2 for the player2
     * @param ingredientId : the ingredient's ID
     * @return  whether the player has shot all the ingredient in his recipe or not
     * if true, it means that player whose ID is playerId won
     */
    public boolean manageShot(int playerId, int ingredientId) {
        if (playerId == 1) {
            // if it's the correct ingredient to shoot
            if (ingredientId == recipe_1[next_ing_1]) {
                // we move to the next ingredient in the recipe
                next_ing_1++;
                // if the player has shot all the ingredients
                if (next_ing_1 == recipe_len_1)
                    return true;
            }
        }
        // we should treat the second player as well later
        return false;
    }

    /**
     * this method draws the recipe in the side of the screen
     * attention : it only draws the ingredients left in the recipe
     * @param mCanvas : the application's canvas
     * @param mPaint : the application's paint
     * @param startY : the Y-position where to draw the recipe
     */
    public void draw(Canvas mCanvas, Paint mPaint, float startY) {
        int ingredientId;
        // r: red, g: green, b: blue
        int r, g, b;
        // the gap between every two successive ingredients
        // in order to not draw ingredients on top of each other
        int gap = ingredient_height;
        int top;
        for (int i = (recipe_len_1 - 1) ; i >= next_ing_1  ; i--) {
            // get the ingredient ID
            ingredientId = recipe_1[i];

            // the ingredient color
            r = IngredientsManager.ingredientsColors[ingredientId][0];
            g = IngredientsManager.ingredientsColors[ingredientId][1];
            b = IngredientsManager.ingredientsColors[ingredientId][2];

            // set the color of the paint
            mPaint.setColor(Color.argb(255, r, g, b));

            // get the top position of the ingredient to draw
            top = (int) (startY - (recipe_len_1 - i) * ingredient_height - (recipe_len_1 - i - 1) * gap);

            // draw the ingredient
            //mCanvas.drawRect(new RectF(ingredient_width, top, 2*ingredient_width, top + ingredient_height), mPaint);
            RectF rectF = new RectF(ingredient_width, top, 2*ingredient_width, top + ingredient_height);
            recipeFrames_1.get(i).draw(mCanvas, Tools.rectFToRect(rectF));
        }

    }
/*
    public void bonusShot(int bonusId) {
        switch (bonusId) {
            case  BonusManager.SHUFFLE:
                Tools.shuffleArray(recipee_1);
                break;
            case  BonusManager.STRIKE:
                for (Ingredient ingredient : )
                break;
            case  BonusManager.BLOCK:
                break;
            case  BonusManager.REVERSE:
                break;
            case  BonusManager.CLEAR:
                break;
        }
    }


 */

}
