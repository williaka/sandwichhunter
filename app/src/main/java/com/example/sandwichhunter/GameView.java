package com.example.sandwichhunter;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.accessibility.AccessibilityManager;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.sandwichhunter.firebase.FirebaseManager;
import com.example.sandwichhunter.firebase.MultiplayerGameManager;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Random;

public class GameView extends SurfaceView implements SurfaceHolder.Callback, Runnable, PropertyChangeListener {
    // This is our thread
    Thread mGameThread = null;

    // tools class contains usefull methods
    private Tools tools;

    // it represents the area of the window the player cannot shoot
    // here we consider only pi/10
    // this means that the player can shoot in
    // 8*PI/10 = [pi/10, 9*pi/10] of the screen
    private static final float blindArea = (float) (Math.PI/10);

    // We need a SurfaceHolder object
    // We will see it in action in the draw method soon.
    SurfaceHolder mOurHolder;

    // A boolean which we will set and unset
    // when the game is running- or not
    // It is volatile because it is accessed from inside and outside the thread
    volatile boolean mPlaying;

    // Game is mPaused at the start
    //boolean mPaused = true;

    // Game startes on start
    boolean mPaused = false;

    // A Canvas and a Paint object
    Canvas mCanvas;
    Paint mPaint;

    // This variable tracks the game frame rate
    long mFPS;

    // The size of the screen in pixels
    int mScreenX;
    int mScreenY;

    // The player mPlayer
    Player mPlayer1;
    //Player mPlayer2;

    // the position of the player
    PointF posPlayer1;
    //PointF posPlayer2;

    // An ingredients manager
    IngredientsManager ingredientsManager;

    // A game manager
    GameManager gameManager;
    //BonusManager bonusManager;

    // For sound FX
    SoundPool sp;
    int beep1ID = -1;
    int beep2ID = -1;
    int beep3ID = -1;
    int loseLifeID = -1;

    Bitmap background;

    // RequiresApi : only necessary for SplittableRandom
    // used in IngredientsManager
    @RequiresApi(api = Build.VERSION_CODES.N)
    public GameView(Context context, int x, int y) {


    /*
        The next line of code asks the
        SurfaceView class to set up our object.
    */
        super(context);

        Constants.CURRENT_CONTEXT = context;

        // Set the screen width and height
        mScreenX = x;
        mScreenY = y;

        // Initialize mOurHolder and mPaint objects
        mOurHolder = getHolder();
        mPaint = new Paint();

        // A new mPlayer
        mPlayer1 = new Player(mScreenX, mScreenY);
        //mPlayer2 = new Player(mScreenX, mScreenY,false);

        posPlayer1 = new PointF(mScreenX/2, 7*mScreenY/8);

        ingredientsManager = new IngredientsManager(mScreenX, mScreenY, (float) (posPlayer1.y - mScreenX/2 * Math.sin(blindArea)));

        tools = new Tools(mScreenX, mScreenY);

        gameManager = new GameManager(mScreenX, mScreenY, IngredientsManager.MAX_ING);

        background = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        background = Bitmap.createScaledBitmap(background, mScreenX, mScreenY, true);

        //bonusManager = new BonusManager(mScreenX, mScreenY, posPlayer1.y);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            sp = new SoundPool.Builder()
                    .setMaxStreams(5)
                    .setAudioAttributes(audioAttributes)
                    .build();

        } else {
            sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        }


        try {
            // Create objects of the 2 required classes
            AssetManager assetManager = context.getAssets();
            AssetFileDescriptor descriptor;

            // Load our fx in memory ready for use
            descriptor = assetManager.openFd("beep1.ogg");
            beep1ID = sp.load(descriptor, 0);

            descriptor = assetManager.openFd("beep2.ogg");
            beep2ID = sp.load(descriptor, 0);

            descriptor = assetManager.openFd("beep3.ogg");
            beep3ID = sp.load(descriptor, 0);

            descriptor = assetManager.openFd("loseLife.ogg");
            loseLifeID = sp.load(descriptor, 0);

            descriptor = assetManager.openFd("explode.ogg");
            int explodeID = sp.load(descriptor, 0);

        } catch(IOException e) {
            // Print an error message to the console
            Log.e("error", "failed to load sound files");
        }

        //setupAndRestart();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(background, 0, 0, null);
    }

    public void setupAndRestart(){
        //Random generator = new Random();
        // Put the mBall back to the start
        //mBall.reset(generator.nextInt(mScreenX), mScreenY/2);
        //mBall.resetVelocity(mScreenY/6, mScreenY/6);
        // if game over reset scores and mLives
        //if(mScoreUp == 5 || mScoreDown == 5) {
        //    mScoreUp = 0;
        //    mScoreDown = 0;
        //}

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void run() {
        this.addPropertyChangeListeners();

        // while the players play
        while (mPlaying) {

            // Capture the current time in milliseconds in startFrameTime
            long startFrameTime = System.currentTimeMillis();

            if (mPlayer1.getScore() > 5) {
                mPlaying = false;
            }


            // Update the frame
            if(!mPaused){
                update();
            }

            // Draw the frame
            draw();

        /*
            Calculate the FPS this frame
            We can then use the result to
            time animations in the update methods.
        */
            long timeThisFrame = System.currentTimeMillis() - startFrameTime;
            if (timeThisFrame >= 1) {
                mFPS = 1000 / timeThisFrame;
            }

        }

        System.out.println("End of game run");

        this.removePropertyChangeListeners();

        MultiplayerGameManager.updatePlayerScore(mPlayer1.getScore());

        //drawEndGame();
        StartGame startGame = (StartGame) Constants.CURRENT_CONTEXT;
        startGame.endGame();

    }

    // Everything that needs to be updated goes in here
    // Movement, collision detection etc.
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void update() {

        // Move the Players if required
        mPlayer1.update(mFPS);
        //mPlayer2.update(mFPS);

        // update the ingredients
        ingredientsManager.update(mFPS);

        //bonusManager.update(mFPS);

        // check whether a shot has collided with an ingredient or not
        for (int i = 0 ; i < mPlayer1.getCurrentShots().size() ; i++) {
            Shot shot = mPlayer1.getCurrentShots().get(i);
            for (int j = 0 ; j < ingredientsManager.getCurrentIngredients().size() ; j++) {
                Ingredient ingredient = ingredientsManager.getCurrentIngredients().get(j);
                if (RectF.intersects(shot.getRect(), ingredient.getRect())) {

                    // Mark it as shot in the database
                    MultiplayerGameManager.updateShotIngredient(ingredient.getIngredientHash());


                    //System.out.println("GM updateShotIngredient success");

                    // if they collided, remove them from the current lists
                    ingredientsManager.ingredientExploded(j);
                    //System.out.println("GM ingredientExploded success");

                    mPlayer1.shotExploded(i, ingredient);
                    //System.out.println("GM shotExploded success");
                    // and check whether the shot was correct or not
                    if (gameManager.manageShot(1, ingredient.getId())) {
                        System.out.println("returning false in collision manageshot");
                        mPlaying = false;
                    }

                    j--;
                    i--;
                    break;
                }
            }
            if (mPlayer1.lost()) {
                mPlaying = false;
                break;
            }
            /*
            if (RectF.intersects(shot.getRect(), bonusManager.getRect())) {
                gameManager.bonusShot(bonusManager.getCurrentBonus());
            }

             */
        }
    }

    // Draw the newly updated scene
    public void draw() {

        // Make sure our drawing surface is valid or we crash
        if (mOurHolder.getSurface().isValid()) {

            // Draw everything here

            // Lock the mCanvas ready to draw
            mCanvas = mOurHolder.lockCanvas();

            // Clear the screen with my favorite color
            //mCanvas.drawColor(Color.argb(255, 0, 0, 0));
            mCanvas.drawBitmap(background, 0, 0, mPaint);

            // Draw the Players
            mPlayer1.draw(mCanvas, mPaint, Color.argb(255, 100, 100, 255));


            // Draw the Ingredients
            ingredientsManager.draw(mCanvas, mPaint, Color.argb(255, 100, 100, 255));

            gameManager.draw(mCanvas, mPaint, posPlayer1.y);

            // Change the drawing color to white
            mPaint.setColor(Color.argb(255, 255, 255, 255));

            // Draw the mScore
            mPaint.setTextSize(40);
            //mCanvas.drawText("ScoreUp: " + mScoreUp + "   ScoreDown: " + mScoreDown, 10, 50, mPaint);
            Tools.drawCenteredTextVertically(mCanvas, mPaint, "Sandwich Hunter", 35, mScreenY / 2);

            // draw the ViewField
            Tools.drawViewField(mCanvas, mPaint, (int) posPlayer1.x, (int) posPlayer1.y, mScreenX, -1, blindArea, false);

            // Draw everything to the screen
            mOurHolder.unlockCanvasAndPost(mCanvas);
        }


    }

    // If the Activity is paused/stopped
    // shutdown our thread.
    public void pause() {
        mPlaying = false;
        try {
            mGameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }

    }

    // If the Activity starts/restarts
    // start our thread.
    public void resume() {
        mPlaying = true;
        mGameThread = new Thread(this);
        mGameThread.start();
    }

    /* not used */
    @Override
    public boolean onHoverEvent(@NonNull MotionEvent event) {
        AccessibilityManager accessibilityManager =
                (AccessibilityManager) getContext().getSystemService(Context.ACCESSIBILITY_SERVICE);
        if (accessibilityManager.isTouchExplorationEnabled()) {
            final int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_HOVER_ENTER:
                    event.setAction(MotionEvent.ACTION_DOWN);
                    break;
                case MotionEvent.ACTION_HOVER_MOVE:
                    event.setAction(MotionEvent.ACTION_MOVE);
                    break;
                case MotionEvent.ACTION_HOVER_EXIT:
                    event.setAction(MotionEvent.ACTION_UP);
                    break;
            }
            onTouchEvent(event);
            event.setAction(action);
        }
        return super.onHoverEvent(event);
    }

    /**
     * calculates the new value of the angle
     * @param motionEvent : the event when the player touches the screen
     * @return the new calculated angle
     */
    public float updateArrow(MotionEvent motionEvent) {
        PointF B1, B2, A1, A2;
        float coordX, coordY;
        float angle;
        float deltaX;

        coordX = motionEvent.getX();
        coordY = motionEvent.getY();

        deltaX = Math.abs(posPlayer1.x - coordX);

        if (coordY >= (posPlayer1.y - deltaX * Math.sin(blindArea)) ) {
            if (coordX >= posPlayer1.x)
                angle = (float) (blindArea);
            else
                angle = (float) (Math.PI - blindArea);
        }
        else {
            B1 = new PointF(coordX, coordY);
            B2 = new PointF(posPlayer1.x, posPlayer1.y);
            A1 = new PointF(posPlayer1.x, posPlayer1.y);
            A2 = new PointF(0, posPlayer1.y);

            angle = Tools.angleBetween2Lines(A1, A2, B1, B2);
        }

        return angle;
    }

    // The SurfaceView class implements onTouchListener
    // So we can override this method and detect screen touches.
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        float angle;

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

            // Player has touched the screen
            case MotionEvent.ACTION_DOWN:
                mPaused = false;
                angle = updateArrow(motionEvent);
                mPlayer1.setArrow(angle);
                mPlayer1.shoot();
                break;

        }
        return true;
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {

    }

    private void addPropertyChangeListeners() {
        MultiplayerGameManager.addPropertyChangeListener(this);
    }

    private void removePropertyChangeListeners() {
        MultiplayerGameManager.removePropertyChangeListener(this);
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getNewValue() instanceof String) {
            if ("status".equals(evt.getPropertyName()) && "ended".equals(evt.getNewValue())) {

                System.out.println("The game has ended");

                mPlaying = false;
            }
            return;
        }
    }

}
