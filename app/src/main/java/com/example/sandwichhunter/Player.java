package com.example.sandwichhunter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

import com.example.sandwichhunter.firebase.MultiplayerGameManager;

import java.util.ArrayList;

public class Player {
    // RectF is an object that holds four coordinates
    private RectF mRect;
    private RectF crossbowRect;

    // How long and high our mPlayer will be
    private float mLength;
    private float mHeight;

    // X is the far left of the rectangle which forms our mPlayer
    private float mXCoord;

    // Y is the top coordinate
    private float mYCoord;

    // This will hold the pixels per second speed that
    // the mPlayer will move
    private float mPlayerSpeed;

    // the angle of the arrow of the player
    // it allows to calculate the trajectory of the shot
    // when the player shoots
    private float arrow;

    /*
    // Which ways can the mPlayer move
    public final int STOPPED = 0;
    public final int LEFT = 1;
    public final int RIGHT = 2;
     */

    // Is the mPlayer moving and in which direction
    //private int mPlayerMoving = STOPPED;

    // The screen length and width in pixels
    private int mScreenX;
    private int mScreenY;

    // stock the currentShots that are in the screen
    private ArrayList<Shot> currentShots;

    // the player's score
    private int mScore;
    // the player's lives left
    private int mLives;

    // the maximum number of lives a player can have initially
    public static final int NB_MAX_LIVES = 5;

    private Bitmap playerFrame;
    private Animation playerAnim;

    private Bitmap crossbowFrame;
    private Animation crossbowAnim;

    private Bitmap shotFrame;

    private boolean lost = false;

    // This is the constructor method
    // When we create an object from this class we will pass
    // in the screen width and mHeight
    public Player(int x, int y){

        mScreenX = x;
        mScreenY = y;

        // 1/8 screen width wide
        mLength = mScreenX / 4;

        // 1/25 screen mHeight high
        //mHeight = mScreenY / 25;
        mHeight = mLength;

        // Start mPlayer in roughly the sceen centre
        mXCoord = mScreenX / 2;
        mYCoord = 7*mScreenY/8;// - 20;

        mRect = new RectF(mXCoord - mLength/2, mScreenY - 20 - mHeight, mXCoord + mLength/2, mScreenY- 20);
        crossbowRect = new RectF(mXCoord - mLength/3, mYCoord - 20 - 3*mHeight, mXCoord + mLength/3, mScreenY - 20 - mHeight);
        //crossbowRect = new RectF(mScreenX/2 - 50, mScreenY/2 - 50, mScreenX/2 + 50, mScreenY/2 + 50);

        // How fast is the mPlayer in pixels per second
        // Cover entire screen in 1 second
        mPlayerSpeed = mScreenX;

        mScore = 0;
        mLives = NB_MAX_LIVES;

        currentShots = new ArrayList<>();

        BitmapFactory bf = new BitmapFactory();

        playerFrame = bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.chef);
        playerAnim = new Animation(playerFrame);
        playerAnim.play();

        crossbowFrame = bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.crossbow);
        crossbowAnim = new Animation(crossbowFrame);
        crossbowAnim.play();

        shotFrame = bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.arrow);
    }

    public float getmLength() { return mLength; }
    public void setmLength(float l) { mLength = l; }
    public float getmHeight() { return mHeight; }
    public void setmHeight(float h) { mHeight = h; }


    // This is a getter method to make the rectangle that
    // defines our player available in GameView class
    public RectF getRect(){
        return mRect;
    }

    public void setArrow(float angle) { arrow = angle; }

    public float getArrow() { return arrow; }

    public ArrayList<Shot> getCurrentShots() { return currentShots; }

    public boolean lost() { return lost; }

    public int getScore() {
        return this.mScore;
    }

    /*
    // This method will be used to change/set if the mPlayer is going
    // left, right or nowhere
    public void setMovementState(int state){
        mPlayerMoving = state;
    }
     */

    // This update method will be called from update in GameView
    // It determines if the Player needs to move and changes the coordinates
    // contained in mRect if necessary
    public void update(long fps){

        /*
        if (mPlayerMoving == LEFT) {
            //mXCoord = mXCoord - mPlayerSpeed / fps;
            arrow -= mPlayerSpeed / fps;
        }

        if (mPlayerMoving == RIGHT) {
            //mXCoord = mXCoord + mPlayerSpeed / fps;
            arrow += mPlayerSpeed / fps;
        }
        */

        // update every shot in the screen
        for (int i = 0 ; i < currentShots.size() ; i++) {
            // if the shot leaves the screen, we remove it
            if (currentShots.get(i).update(fps)) {
                currentShots.remove(i);
                i--;
            }
        }

        crossbowAnim.setFrame(Tools.rotateBitmap(crossbowFrame, (float)(45 - arrow * 180 / Math.PI)));
    }

    /**
     * draws the player, the current shots, the score and the lives left
     * @param mCanvas
     * @param mPaint
     * @param argb : the color
     */
    public void draw(Canvas mCanvas, Paint mPaint, int argb) {
        //mPaint.setColor(Color.argb(Color.alpha(argb), Color.red(argb), Color.green(argb), Color.blue(argb)));
        //mCanvas.drawRect(this.crossbowRect, mPaint);
        playerAnim.draw(mCanvas, Tools.rectFToRect(mRect));
        crossbowAnim.draw(mCanvas, Tools.rectFToRect(crossbowRect));

        mPaint.setTextSize(40);
        // 8 = size of the text to draw
        Tools.drawCenteredText(mCanvas, mPaint, "score:" + mScore, mScreenX/8, mScreenY/40);
        Tools.drawCenteredText(mCanvas, mPaint, "lives:" + mLives, mScreenX/8, 2*mScreenY/40);

        mPaint.setColor(Color.argb(255, 255, 0, 0));
        for (Shot shot : currentShots) {
            //mCanvas.drawRect(shot.getRect(), mPaint);
            shot.draw(mCanvas);
        }
    }

    /**
     * removes the shot from currentShots
     * @param index : the index of the shot in the list
     */
    public void removeShot(int index) {
        currentShots.remove(index);
    }

    /**
     * called when a shot collides with an ingredient
     * @param index : the index of the shot in the list
     * @param shotIngredient : the ID of the ingredient that was shot
     */
    public void shotExploded(int index, Ingredient shotIngredient) {
        // if the ingredient is rotten
        // the player looses a life
        if (shotIngredient.isRotten()) {
            mLives--;
            if (mLives <= 0) {
                System.out.println("You loose !");
                lost = true;
            }
        }
        // else he gains in score
        else {
            mScore++;
        }

        // remove the shot from the currentShots list
        currentShots.remove(index);

    }

    /**
     * called when the player shoots i.e if the screen is touched
     * generate a new shot and add it to the currentShots list
     */
    public void shoot() {
        currentShots.add(new Shot(mRect.left + mLength/2, mRect.top, arrow, mScreenX, mScreenY, shotFrame));
    }
}
