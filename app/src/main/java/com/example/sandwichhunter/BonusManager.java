package com.example.sandwichhunter;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import java.util.Random;

enum Bonus { NO_BONUS, SHUFFLE, STRIKE, BLOCK, REVERSE, CLEAR};
enum SpecialBonus { NO_BONUS, STRIKE, LOOSE2, ADD2};

public class BonusManager {
    private final int[][] bonus = new int[][] {
            {255, 100, 50},
            {50, 100, 255},
            {50, 255, 100},
            {100, 255, 50}
    };

    private final int[][] specialBonus = new int[][] {
            {60, 150, 60},
            {60, 60, 150},
            {150, 60, 60},
            {100, 255, 50}
    };

    private final int NB_BONUS = 5;

    public final int NO_BONUS = 0;
    public static final int SHUFFLE = 1;
    public static final int STRIKE = 2;
    public static final int BLOCK = 3;
    public static final int REVERSE = 4;
    public static final int CLEAR = 5;

    public static final int LOOSE2 = 6;
    public static final int ADD2 = 7;


    private int mScreenX, mScreenY;
    private long lastFrameTime;
    private Random bonusGenerator;
    private Random positionGenerator;

    private int currentBonus;
    private RectF mRect;

    private int mWidth, mHeight;

    private float limitY;
    private int maxPosX, minPosX;
    private float mVelocity;

    private boolean chrono;

    public BonusManager(int mScreenX, int mScreenY, float limitY) {
        this.mScreenX = mScreenX;
        this.mScreenY = mScreenY;

        this.limitY = limitY;

        mWidth = mScreenX/30;
        mHeight = mWidth;

        mVelocity = mScreenY/10;

        // Capture the current time in milliseconds in startFrameTime
        lastFrameTime = System.currentTimeMillis();

        bonusGenerator = new Random();
        positionGenerator = new Random();

        int random_position = positionGenerator.nextInt(maxPosX - minPosX) + minPosX;
        mRect = new RectF(random_position, 0, random_position + mWidth, mHeight);
        chrono = true;
    }

    public RectF getRect() { return mRect; }

    public int getCurrentBonus() { return currentBonus; }

    public void update(long fps) {
        // Capture the current time in milliseconds in startFrameTime
        long currentFrameTime = System.currentTimeMillis();

        if (!chrono) {
            mRect.top = mRect.top + (mVelocity / fps);
            mRect.bottom = mRect.top + mHeight;
            if (mRect.bottom >= limitY) {
                currentBonus = NO_BONUS;
                chrono = true;
            }
        }
        // every 5 sec
        else if ((currentFrameTime - lastFrameTime) >= 5000) {
            int bonusId = bonusGenerator.nextInt(15);
            if (bonusId > 0 && bonusId < 5)
                currentBonus = SHUFFLE;
            else if (bonusId >= 5 && bonusId < 9)
                currentBonus = STRIKE;
            else if (bonusId >= 9 && bonusId < 12)
                currentBonus = BLOCK;
            else if (bonusId >= 12 && bonusId < 14)
                currentBonus = REVERSE;
            else if (bonusId == 14)
                currentBonus = CLEAR;

            chrono = false;
        }
    }

    public void draw(Canvas mCanvas, Paint mPaint) {
        if (currentBonus != NO_BONUS) {
            mPaint.setColor(Color.argb(255, bonus[currentBonus][0], bonus[currentBonus][1], bonus[currentBonus][2]));
            mCanvas.drawRect(mRect, mPaint);
        }
    }
}
