package com.example.sandwichhunter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sandwichhunter.firebase.MultiplayerGameManager;
import com.example.sandwichhunter.firebase.MultiplayerPlayer;

public class MainActivity extends AppCompatActivity {

    private Button playButton;
    private Button joinButton;
    private Button tutorialButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playButton = findViewById(R.id.playButton);
        joinButton = findViewById(R.id.joinButton);
        tutorialButton = findViewById(R.id.tutorialButton);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createGame();
            }
        });

        joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                joinGame(view);
            }
        });

        tutorialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTutorial(view);
            }
        });

        // Insert previous username into username field if available
        MultiplayerPlayer multiplayerPlayer = MultiplayerGameManager.getPlayer();
        if (multiplayerPlayer != null) {
            String username = multiplayerPlayer.getUsername();
            setUsernameFieldValue(username);
        }
    }

    private void joinLobby() {
        Intent intent = new Intent(MainActivity.this, LobbyActivity.class);
        startActivity(intent);
    }

    private void createGame() {

        String username = getUsername();
        if ("".equals(username)) {
            return;
        }

        configureDatabase(username);

        MultiplayerGameManager.createGameInstanceInFirebase();

        joinLobby();
    }

    private void configureDatabase(String username) {
        MultiplayerPlayer multiplayerPlayer = new MultiplayerPlayer(username);
        MultiplayerGameManager.setFirebaseManager(multiplayerPlayer);
    }

    private String getUsername() {

        EditText usernameField = findViewById(R.id.username);
        String username = usernameField.getText().toString();


        if (username.length() < 6) {
            usernameField.setBackgroundColor(Color.parseColor("#29FF0000"));
            return "";
        }

        return username;
    }

    private void setUsernameFieldValue(String username) {
        EditText usernameField = findViewById(R.id.username);
        usernameField.setText(username);
    }

    private void joinGame(View view) {

        String username = getUsername();
        if ("".equals(username)) {
            return;
        }

        configureDatabase(username);

        Intent intent = new Intent(MainActivity.this, JoinActivity.class);
        startActivity(intent);
    }

    private void showTutorial(View view) {
        Intent intent = new Intent(MainActivity.this, TutorialActivity.class);
        startActivity(intent);
    }
}
