package com.example.sandwichhunter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.sandwichhunter.firebase.AvailableGame;
import com.example.sandwichhunter.firebase.MultiplayerGameManager;
import com.example.sandwichhunter.firebase.MultiplayerPlayer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

public class LobbyActivity extends AppCompatActivity implements PropertyChangeListener {

    private Button backButton;
    private Button startGameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);

        this.backButton = findViewById(R.id.backButton);
        this.startGameButton = findViewById(R.id.startGameButton);

        ProgressBar pgsBar = (ProgressBar)findViewById(R.id.pBar);
        //pgsBar.setVisibility(v.GONE);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preLeaveLobbyChecks();
                removePropertyChangeListeners();
                leaveLobby();
            }
        });

        startGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableStartGame();
                removePropertyChangeListeners();
                startGame();

            }
        });

        updatePlayers(MultiplayerGameManager.getPlayers());

        if (MultiplayerGameManager.playerIsHost()) {
            MultiplayerGameManager.updateStatus("lobby");
        }

        addPropertyChangeListeners();
    }

    private void startGame() {
        Intent intent = new Intent(LobbyActivity.this, StartGame.class);
        startActivity(intent);
    }

    private void preLeaveLobbyChecks() {
        // Check if player is game host, end game if they are
        if (MultiplayerGameManager.playerIsHost()) {
            MultiplayerGameManager.removeGameInstanceInFirebase();
        } else {
            MultiplayerGameManager.leaveGame();
        }
    }

    private void leaveLobby() {
        Intent intent = new Intent(LobbyActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void updatePlayers(ArrayList<MultiplayerPlayer> players) {

        List<String> usernames = new ArrayList<>(players.size());
        for (MultiplayerPlayer player : players) {
            usernames.add(Objects.toString(player.getUsername() + " joined the game!", ""));
        }

        ListView listView = (ListView) findViewById(R.id.listview);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, usernames);
        listView.setAdapter(arrayAdapter);

        if (MultiplayerGameManager.playerIsHost() && players.size() >= 2) {
            enableStartGame();
        } else {
            disableStartGame();
        }
    }

    private void enableStartGame() {
        this.startGameButton.setEnabled(true);
    }

    private void disableStartGame() {
        this.startGameButton.setEnabled(false);
    }

    private void addPropertyChangeListeners() {
        MultiplayerGameManager.addPropertyChangeListener(this);
    }

    private void removePropertyChangeListeners() {
        MultiplayerGameManager.removePropertyChangeListener(this);
    }

    public void propertyChange(PropertyChangeEvent evt) {

        if (evt.getNewValue() == null) {
            if ("host".equals(evt.getPropertyName())) {
                System.out.println("Leaving do to null");
                leaveLobby();
            }
            return;
        }

        if (evt.getNewValue() instanceof String) {
            if ("status".equals(evt.getPropertyName()) && "started".equals(evt.getNewValue())) {
                disableStartGame();
                removePropertyChangeListeners();
                startGame();
            }
            return;
        }

        if (evt.getNewValue() instanceof ArrayList) {
            ArrayList<MultiplayerPlayer> players = (ArrayList<MultiplayerPlayer>) evt.getNewValue();
            this.updatePlayers(players);
            return;
        }
    }
}